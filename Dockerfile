ARG NODEJS="18"
ARG REVEALJS="4.6.0"

FROM node:${NODEJS}-alpine
ARG NODEJS
ARG REVEALJS

RUN apk add --no-cache vim wget zip unzip

RUN wget -O reveal.js-${REVEALJS}.zip https://codeload.github.com/hakimel/reveal.js/zip/refs/tags/${REVEALJS}
RUN unzip /reveal.js-${REVEALJS}.zip
RUN mv /reveal.js-${REVEALJS} /app

COPY src/index.html /app/index.html
COPY src/ansilabs.css /app/ansilabs.css
COPY src/assets /app/assets
COPY src/content /app/contents

RUN cd /app && npm ci

EXPOSE 8000 35729
WORKDIR /app

CMD ["npm", "start", "--", "--host=0.0.0.0"]
