# Video

Welcome to ansi labs' tiny guide for video production. It is currently aimed at people working in content creation and marketing. ***The contents were in German at the time.*** However, updates, an English version, and additional technical sections are possible in the future.

## Development

You need an editor supporting Markdown and [Docker](https://www.docker.com) installed. To build the Docker image run:

```bash
docker build -t ansilabs/video .
```

After building the Docker image, run the container:

```bash
docker run -d --name "alsvid" -p 8000:8000 -p 35729:35729 -v $(pwd)/src/index.html:/app/index.html -v $(pwd)/src/ansilabs.css:/app/ansilabs.css -v $(pwd)/src/assets:/app/assets -v $(pwd)/src/content:/app/content ansilabs/video
```

To open the development URL, open <http://localhost:8000> in your browser. Note that you can replace "ansilabs/video" and "alsvid" with values of your choice.

## License

MIT, please refer to [LICENSE](./LICENSE) for the details; The content is licensed under Attribution 4.0 International. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

Copyright (c) 2023–present ansi labs GmbH, and Contributors

