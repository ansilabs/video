# Plattformen


Es gibt eine ganze Reihe von Möglichkeiten, Videos im Internet zu veröffentlichen. Für den Augenblick bleiben wir bei drei Optionen:

1. Auf dem eigenen Server
2. Bei einer spezifischen Plattform, zum Beispiel Facebook
2. Bei einer spezifischen Video-Sharing Plattform wie Vimeo oder YouTube


## Videos auf einem eigenen Server hosten

TEXTCONTENT:
Manchmal ist es wichtig, dass die Kontrolle über ein Video ganz bei dir bleibt. In diesem Fall bleibt dir fast nur das Hosten auf einem Server, der dir gehört. Grosse Web-CMS wie WordPress oder Drupal bieten den Upload von Videos an. Was du in diesem Augenblick brücksichtigen musst, ist, dass die Videoausgabe im Internet schnell viele Ressourcen binden kann. Du musst also sicher sein, dass deine Server genügend Leistung haben, um Video-Dateien zu verarbeiten und auszuliefern.


## Videos bei einer spezifischen Plattform veröffentlichen

TEXTCONTENT:
Wenn du mit deiner Kommunikation ganz stark auf eine Plattform fokussiert bist, kannst du deine Videos direkt dort hochladen. Beachte aber, dass ein Video bei Facebook, Instagram oder LinkedIn nicht unbedingt für deine gesamte Kundschaft sichtbar ist – zum Beispiel weil sie auf der Plattform deiner Wahl kein Login hat.


## Auf einer spezifischen Video-Sharing Plattform veröffentlichen

TEXTCONTENT:
Es gibt weltweit zwei grosse Video-Sharing-Plattformen: Vimeo und YouTube. Der wesentliche Vorteil dieser Plattformen kann sein, dass du das Video schnell und unkompliziert bei weiteren Plattformen und auf deiner Webseite einbinden kannst.

YouTube bietet dir vor allem die Möglichkeit, ein vielfältiges Publikum zu erreichen. Wenn dein Video richtig gut zieht, wird es schnell sehr viele Menschen geben, die es anschauen – du gehst «vial». Wenn dein Video aber nicht so viel Beachtung erhält, wird es schnell zwischen den vielen anderen Videos auf der Plattform verschwinden.

Mit Vimeo hast du eine bessere Kontrolle darüber, wer dein Video sehen darf (Shelton, 2023). So kannst du zum Beispiel Vimeo als deinen privaten Video-Streaming-Service einsetzen. Das heisst, du kannst dein Video von Vimeo über eine private Leitung in deine Webseite einbinden, ohne dass das Video auf der Plattform öffentlich oder versteckt (nicht gelistet) sein muss.

Während YouTube YouTubern die Möglichkeit bietet, viral zu gehen und ein vielfältiges Publikum zu erreichen, gibt es auf der Website so viele Inhalte, dass sie leicht im Meer der Videos untergehen könnten. Mit Vimeo haben Sie eine bessere Kontrolle darüber, wer Ihre Arbeit sehen darf und wie sie Ihr Publikum erreicht.

Für professionelle Arbeiten ist Vimeo oft die richtig Wahl, weil zum Beispiel keine Werbung angehängt wird, wenn du einen bezahlen Account hast. Wenn du dagegen schnell eine allgemeine Bekanntheit erreichen willst, bist du wahrscheinlich mit YouTube erfolgreicher.
