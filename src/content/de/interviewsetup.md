# Setup für Interview und Statements


![Kameras bei Interview](../../assets/inverviewsetup/fj-53.png)

<small>Das Setup einer typischen Interview-Situation (Schult & Buchholz, 2011, S. 53).</small>

TEXTCONTENT:
Wie viele Personen sollen im Bild sein? In der Regel werden in Interviews die Interviewpartner nicht aber die Redaktoren gezeigt.

In dieser Komplettsituation wird mit drei Kameras gearbeitet. Es ist aber auch möglich, mit einer Kamera alle drei Positionen einzunehmen.

Kamera 1 zeigt den Redaktor fokussiert aber auf den Interviewpartner. Das kann manchmal nützlich sein – vor allem bei langen Interviews. Kamera 2 und 3 haben exklusive den Interviewpartner im Blick. In diesem Kontext ist die Kontinuität der Blickrichtung entscheidend (Schult & Buchholz, 2011, S. 53). Es darf keinen Achsensprung geben. Wenn während dem Interview mit der gleichen Person von Kamera 2 auf Kamera 3 geschnitten wird, ändert die Person im Bild aus dem Nichts ihre Blickrichtung. Das verwirrt am Bildschirm wie auch im realen leben. Vielmehr Kamera 2 und 3 abwechselnd für Interviewpartner genutzt werden, um einen guten Anschluss zu gewährleisten.


![Bildwechsel bei Interview](../../assets/inverviewsetup/fj-100.png)

<small>Korrekter und falscher Bildwechsel (Schult & Buchholz, 2011, S. 100).</small>


## Ich muss ein Interview kürzen, was soll ich tun?

TEXTCONTENT:
Wenn Du ein Interview kürzen musst, hast du zwei Möglichkeiten.

1. Du beschaffst dir Füllbilder zum Text des Interviewpartners. Wenn dieser zum Beispiel über Autos spricht, machst du ein paar Aufnahmen von Autos auf der Strasse. Im Schnitt ersetzt du im Zeitabschnitt um die Schnittposition das Bild des Interviewers mit den Füllbildern und schneidest darunter die Audiospur zusammen. Danach zeigst du wieder deinen Interviewpartner.
2. In der Regel üblicher als Füllbilder ist das Einsetzen einer Weissblende. Dabei wird das Bildmaterial mit dem Auto auf die länger zusammengeschnitten, die du brauchst. An der Schnittposition fügst du über einige wenige Bilder hinweg einen Übergang vom Bild zu Weiss und zurück zum Bild ein. Dadurch kann das Bild nicht springen (Shanul et al., o. J.).
