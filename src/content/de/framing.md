# Einstellungsgrössen

TEXTCONTENT:
Die Einstellungsgrössen orientieren sich an der Grösse des Menschen oder des Objekts im Bild. Es gibt verschiedene Definitionen für die Einstellungsgrössen. Axel Buchholz geht in seinem Buch « Fernseh-Journalismus: ein Handbuch für Ausbildung und Praxis» zum Beispiel von fünf Einstellungsgrössen aus (S. 30). In meiner Praxis bin ich oft acht Einstellungsgrössen begegnet. Es sind das zusammengefasst:


| Totale Einstellungen | Nahe Einstellungen
|----------------------|-------------------
| 1. Supertotale       | 5. Halbnahe
| 2. Totale            | 6. Nahe
| 3. Halbtotale        | 7. Grosse
| 4. Amerikanisch      | 8. Detail

TEXTCONTENT:
Eine totale Einstellung ist eher offen, während eine nahe Einstellung auf einen Ausschnitt fokussiert.


## Supertotale Einstellung

![Beispiel Supertotale](../../assets/framing/einstellungsgroesse_supertotale.png)

<small>Blick über Zürich</small>

TEXTCONTENT:
Bei der supertotalen Einstellung wird «das grosse Ganze» gezeigt. Zum Beispiel der Blick über eine Stadt.


## Totale Einstellung

![Beispiel Totale](../../assets/framing/einstellungsgroesse_totale.png)

<small>Die Gesamtansicht am Paradeplatz Zürich</small>

TEXTCONTENT:
Die totale Einstellung fokussiert auf die Gesamtansicht einer Situation.


## Halbtotale Einstellung

![Beispiel Totale](../../assets/framing/einstellungsgroesse_halbtotale.png)

<small>Diese Halbtotale fokussiert auf die Menschen im Vordergrund.</small>

TEXTCONTENT:
Die Halbtotale zeigt einen Ausschnitt einer Situation.


## Amerikanische Einstellung

![Beispiel Amerikanische](../../assets/framing/einstellungsgroesse_amerikanische.png)

<small>Der Oberkörper mit Hüfte und Revolver.</small>

TEXTCONTENT:
Der «Cowboy-Shot» ... Der «Cowboy-Shot» hat seinen Ursprung in den amerikanischen Western Filmen. Er fokussiert auf die obere Körperhälfte aber und inkludiert die Hüfte und den Revolver (Chen, 2021).


## Halbnahe Einstellung

![Beispiel Halbnahe](../../assets/framing/einstellungsgroesse_halbnahe.png)

<small>Bei Menschen zeigt die halbnahe Einstellung den Oberkörper. Sie wird gerne für Interviews verwendet.</small>

TEXTCONTENT:
Die halbnahe Einstellung geht etwas näher an ein Objekt heran als die amerikanische Einstellung.


## Nahe Einstellung

![Beispiel Nahe](../../assets/framing/einstellungsgroesse_nahe.png)

<small>Die nahe Einstellung wird gerne bei Interviews verwendet. In der Bildgestaltung wird der Kopf oben entweder leicht angeschnitten oder der Kameramann lässt oben zwischen 3% und 5% «Luft» zum Bildrand.</small>

TEXTCONTENT:
Die nahe Einstellung grenzt das Sichtfeld weiter ein.


## Grosse Einstellung (Grossaufnahme)

![Beispiel Grossaufnahme](../../assets/framing/einstellungsgroesse_grosse.png)

<small>Diese Einstellung grenzt das Sichtfeld gegenüber einer nahen Einstellung weiter ein und setzt einen klaren Fokus.</small>

TEXTCONTENT:
Die grosse Einstellung macht eine Grossaufnahme von einem Objekt.


## Detail Einstellung

![Beispiel Detail](../../assets/framing/einstellungsgroesse_detail.png)

<small>In diesem Beispiel liegt der Fokus der Detaileinstellung auf den Augen und dem Mund.</small>

TEXTCONTENT:
Die Detaileinstellung zeigt ein Objekt im Detail.
