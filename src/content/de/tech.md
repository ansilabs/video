# Videotechnik


## Bildauflösung

TEXTCONTENT:
Wir beginnen mit der Bildauflösung. Über die Jahre sind eine ganze Reihe von Auflösungen entstanden. Heute geläufig sind vor allem die Begriffe HD, FullHD, UHD oder 2k und 4k. Während die Begriffe mit «HD» im Namen eine Verbindung zum HD-Standard haben, orientieren sich Begriffe 2k oder 4k an der Bildauflösung. HD steht für «High Definition» und wird in verschiedenen ISO-Standards für Bild und Ton definiert.


### Seitenverhältnis

![HD Formate](../../assets/tech/hdformat.png)

<small>Die gängigen Video-Formate (Quelle: DS Play, 2020).</small>

TEXTCONTENT:
Mit der Digitalisierung hat sich auch im Videobereich das Seitenverhältnis 16:9 durchgesetzt (Bühler et al., 2018, S. 43).


### Bildaufbau

![AV Medien Bildaufbau](../../assets/tech/av-medien-36.png)

<small>Interlaced vs. Progressiv (Quelle: Bühler et al., 2018, S. 36)</small>

TEXTCONTENT:
Für den Aufbau von Video-Bildern am Bildschirm sind zwei Verfahren geläufig: Interlace und Progressiv. Beim «Interlace»-Verfahren wird das Bild in Zeilen aufgeteilt. Der Bildschirm zeigt pro Sekunde je einmal alle geraden und ungeraden Zeilen. Der Bildwechsel geht so schnell, dass das menschliche Auge nur ein Bild sieht (Bühler et al., 2018, S. 36).

Das Interlace-Verfahren kommt aus den Anfängen des Fernsehens. Damals konnten Bilder noch nicht ohne Weiteres in hoher Auflösung übertragen werden. Es findet heute kaum noch Anwendung.

Das heute geläufige Verfahren für den Bildaufbau ist progressiv. Das heisst, der Bildschirm zeigt das ganze Bild an.


### Bildrate

![AV Medien Bildrate](../../assets/tech/av-medien-43.png)

<small>Die gängigen Bildraten (Quelle: Bühler et al., 2018, S. 43)</small>

TEXTCONTENT:
Die Bildrate beschreibe die Anzahl Bilder, welche pro Sekunde dargestellt werden. Sie wird mit begriffen wie «25p» oder «50i» bezeichnet. «25p» bedeutet, dass 25 Bilder pro Sekunde im progressiven Verfahren angezeigt werden. «50i» meint, dass 50 Halbbilder – also 25 Vollbilder / 2 – pro Sekunde angezeigt werden (Bühler et al., 2018, S. 43).

Genau wie die Bildauflösung ist auch die Bildrate normiert. In Europa wird nach dem PAL-System gearbeitet. PAL steht für «Phase Alternating Line» und geht von 25 Vollbildern oder 50 Halbbildern pro Sekunde aus (Bühler et al., 2018, S. 43).

Amerikanische Plattformen wie YouTube haben aber auch das amerikanische System NTSC in Europa weit verbreitet. Es unterscheidet sich darin, dass es von 29.97 Bildern (59.94 Halbbildern) pro Sekunde ausgeht (Jacklin, 2023). Die Bildrate wird aber oft auch einfach mit 30p oder 60i ausgewiesen.


## Der Codec

TEXTCONTENT:
Der Begriff «Codec» ist ein Kunstwort und aus dem englischen «Compression/Decompression» abgeleitet. Er beschreibt das Verfahren bei dem Video- und Audiodaten in eine Datei verpackt und entpackt werden (Bühler et al., 2018, S. 45).


## Containerformate

![AV Medien Bildaufbau](../../assets/tech/av-medien-48.png)

<small>Der Bildaufbau (Quelle: Bühler et al., 2018, S. 48)</small>

TEXTCONTENT:
Die eigentliche Videodatei ist ein Container, gefüllt mit Video-, Audio und Metadaten. In gewisser Weise ist sie vergleichbar mit einer PDF-Datei, die Texte, Bilder und Schriften umfasst. Aber Vorsicht: zwei Videodateien können die gleiche Endung haben, obwohl sie nicht die gleichen Codecs enthalten. So kann es zum Beispiel sein, dass sich von zwei Dateien mit der Endung «.mp4» nur eine abspielen lässt. Grund: dem Player fehlt der Decoder für eine der beiden Dateien (Bühler et al., 2018, S. 48).
