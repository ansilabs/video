# Interviews und Statements richtig vorbereiten


Ein Interview oder Statement wird gerne auch als O-Ton bezeichnet. Das «O» steht dabei «Original» und meint eine kurze, authentische Wort-Aufnahme (Schult & Buchholz, 2011, S. 102).

TEXTCONTENT:
Kurz und authentisch meint, dass in wenig Zeit mit wahrscheinlich knappen Worten eine möglichst präzise Aussage gemacht werden soll. Was im Fernseher eher spontan sein darf, muss im Corporate Publishing präzise vorbereitet sein. Der Interviewpartner soll genau die Antwort geben, die zum Kunden transportiert werden soll. Das erfordert eine genaue Planung und eine detaillierte Vorbereitung. Für Marketingverantwortliche gilt: wer gute Aufnahmen will, ist selbst vor Ort!


## Vorbereitung


Welche Nachricht, welche Geschichte will das Unternehmen transportieren? Schränke dich ein, fokussiere auf eine oder zwei zentrale Aussagen pro Statement. Bereite diese genau vor.


Wie ist die Situation am Aufnahmeort?

TEXTCONTENT:
Der Aufnahmeort hängt vom Inhalt der Frage ab (Schult & Buchholz, 2011, S. 98). Bei Aufnahmen an einer Konferenz ist es in der Regel für Statements ratsam, vor den Aufnahmen eine ruhige Ecke ausfindig zu machen. Wenn ich dagegen eine Frage über die Zuverlässigkeit einer Maschine im alltäglichen Betrieb habe, ist es gut, wenn diese im Hintergrund zu sehen und zu hören ist.


Stimmt der optische Hintergrund?

TEXTCONTENT:
Findet ein Interview zum Beispiel auf einer Tagung statt, will ich wenn möglich keine Logos der Konkurrenz im Hintergrund. Falls das nicht möglich ist, kann ich sie vielleicht mit einem Roll-up oder einer Interview-Wand mit dem eigenen Logo verbergen.


Wer sind meine Befragten?

TEXTCONTENT:
Die Auswahl der Befragten muss überlegt und gezielt passieren. Passen meine Interviewpartner zur Frage, die ich stellen will? Exotische oder exzentrische Interviewpartner können mehr Aufmerksamkeit erzeugen. Da der Zuschauer aber automatisch das Aussehen mit dem Inhalt in Beziehung setzt, werden solche Interviewpartner nicht immer gleich ernst genommen.


Wie viele Befragte habe ich?

TEXTCONTENT:
Bei mehreren Befragten ist es oft ratsam abwechselnd von leicht links und rechts aufzunehmen (Schult & Buchholz, 2011, S. 99).


Stelle ich die richtige Frage und kann ich meinen Interviewpartner darauf vorbereiten?

TEXTCONTENT:
Wenn ich meine Interviewpartner spontan auswähle, muss er die Frage ohne Mühe verstehen können. Sie darf also weder zu lange noch inhaltlich zu komplex sein. In der Regel ist es im Corporate Publishing aber besser, wenn ich meine Interviewpartner vorab briefen kann. Damit können die Interviewpartner ihre Antworten vorbereiten und präzisieren. Weitere wichtige Elemente sind (Schult & Buchholz, 2011, S. 101–102):

- Keine Suggestiv-Fragen die Ja/Nein-Antworten ermöglichen.
- Keine verschachtelten Fragen.
- Keine Bestätigungslaute wie «Ja, ja» während dem die befragte Person spricht.
- Habe ich einen Drehplan mit Spannungsbogen?
- Zeichne deine Idee auf, bevor du sie realisierst.
- Wo genau soll ungefähr die Hauptaussage zu liegen kommen? Am Anfang des Statements oder am Ende? Stimmen deine Fragen mit der Platzierung des Höhepunktes überein?
- Hast du organisatorisch und administrativ an alles gedacht? Sind zum Beispiel Drehgenehmigungen nötig?


Habe ich eine Drehplanung?


## Durchführung

TEXTCONTENT:
Bei jeder Durchführung prasseln in kurzer Zeit sehr viele Informationen auf dich ein. Ist der Interviewpartner da? Ist er nervös? Stimmt die Position der Kamera usw. Viele Arbeiten ergeben sich aus der Situation. Trotzdem empfehle ich dir, auf einige wenige Dinge ganz genau zu achten.


`1.` Stimmt der Kontext im Text?

TEXTCONTENT:
Achte ganz genau auf die Antwort deines Gegenübers:

- Sind die Sätze klar und geschlossen?
- Hat es keine oder nur wenige Verlegenheitslaute wie «äh» und «hm»?
- Kann die Antwort für sich alleine stehen?

Insbesondere der dritte Punkt ist essenziell wichtig. Wenn du deinen Interviewpartner fragst: «Wie laufen die Server?», und er antwortet: «Sie laufen gut.», ist für den späteren Zuschauer nicht klar, was gut läuft. Für Statements ist es oft essenziell, dass die Befragten die Frage in ihrem Satz aufnehmen: «Die Server laufen gut.»

Falls du der Meinung bist, dass deine Aufnahme nicht gelungen ist, scheue dich nicht, eine zweite oder dritte Aufnahme zu machen. Das gibt dir die Möglichkeit, durch Übung beim Gegenüber ein besseres Ergebnis zu erzielen. Falls das nicht klappt hast du immer noch die Option, im Schnitt Passagen aus mehreren Aufnahmen zusammenzufügen.


`2.` Habe ich das Recht an Bild und Wort?

TEXTCONTENT:
Das musst du strickt beachten. Nur wer explizit, vielleicht sogar schriftlich, zugestimmt hat, darf aufgenommen und publiziert werden. Informiere deine Interviewpartner ganz genau, was du machen willst (Schult & Buchholz, 2011, S. 102).
